$(document).ready(function () {
  /**
   * Авторизация пользователя в системе
   */
  $("form#FormSignIn").submit(function (e) {
    e.preventDefault();

    var action = $(this).attr("action");
    let formSerialize = $(this).serialize();
    $.ajax({
      url: action,
      type: "POST",
      data: formSerialize,
      dataType: "json",
      success: function (res) {
        // Ошибка при авторизации
        if (res.status == 0) {
          $(".card-error .card-text").html(res.data);
          $(".card-error").removeClass("hidden");
        }
        // Пользователь авторизирован
        if (res.status == 1) {
          window.location = "/";
        }
      },
      error: function (jqXHR, exC1eption) {
        console.log("Error!");
        console.log(jqXHR);
        console.log(exC1eption);
      },
    });
  });

  /**
   * Регистрация пользователя в системе
   */
  $("form#formReg").submit(function (e) {
    e.preventDefault();

    var isValid = true;
    var textValid = "";
    $("form#formReg input").removeClass("is-invalid");
    $("#CardRegError").addClass("hidden");

    var password = $("input[name=password]").val();
    var passwordRepeat = $("input[name=password-repeat]").val();
    // Проверка пароля на минимальный размер
    if (password.length < 6) {
      isValid = false;
      textValid = "Пароль должен содержать не менее 6 символов";
      $("input[name=password]").addClass("is-invalid");
    }
    // Проверка введеных паролей
    if (password != passwordRepeat) {
      isValid = false;
      textValid = "Пароли не совпадают";
      $("input[name=password]").addClass("is-invalid");
      $("input[name=password-repeat]").addClass("is-invalid");
    }

    if (!isValid) {
      $("#CardRegError .card-text").html(textValid);
      $("#CardRegError").removeClass("hidden");
      return;
    }

    // Отправка данных на регистрацию
    let formSerialize = $(this).serialize();
    $.ajax({
      url: "ajax.php?f=ajaxReg",
      type: "POST",
      data: formSerialize,
      dataType: "json",
      success: function (res) {
        console.log(res);
        // Пользователь с таким почтовым адресом уже зарегистрирован
        if (res.status == 0) {
          $("#CardRegError .card-text").html(res.data);
          $("#CardRegError").removeClass("hidden");
        }
        // Пользователь зарегистрирован
        if (res.status == 1) {
          window.location = "/";
        }
      },
      error: function (jqXHR, exC1eption) {
        console.log("Error!");
        console.log(jqXHR);
        console.log(exC1eption);
      },
    });
  });

  /**
   * Окно выбора преподавателя
   */
  $("input[name=role]").change(function (e) {
    e.preventDefault();
    if ($("#radioTeacher").is(":checked")) {
      $("#ContainerSelectTeachers").addClass("disabled-hidden");
    }
    if ($("#radioStudent").is(":checked")) {
      $("#ContainerSelectTeachers").removeClass("disabled-hidden");
    }
  });



  /**
   * Обновление данный профиля
   */
  $("#FormUpdateUser").submit(function (e) {
    e.preventDefault();

    var isValid = true;
    var textValid = "";
    $(this).find("input").removeClass("is-invalid");
    $(this).find("#CardRegError").addClass("hidden");
    $(this).find("#CardRegSuccess").addClass("hidden");

    var password = $(this).find("input[name=password]").val();
    var passwordRepeat = $(this).find("input[name=password-repeat]").val();
    // Проверка пароля на минимальный размер
    if (password.length < 6) {
      isValid = false;
      textValid = "Пароль должен содержать не менее 6 символов";
      $(this).find("input[name=password]").addClass("is-invalid");
    }
    // Проверка введенных паролей
    if (password != passwordRepeat) {
      isValid = false;
      textValid = "Пароли не совпадают";
      $(this).find("input[name=password]").addClass("is-invalid");
      $(this).find("input[name=password-repeat]").addClass("is-invalid");
    }

    if (!isValid) {
      $(this).find("#CardRegError .card-text").html(textValid);
      $(this).find("#CardRegError").removeClass("hidden");
      return;
    }

    // Отправка данных на регистрацию
    var action = $(this).attr("action");
    let formSerialize = $(this).serialize();
    $.ajax({
      url: action,
      type: "POST",
      data: formSerialize,
      dataType: "json",
      success: function (res) {
        // Обновление данный прошло с ошибкой
        if (res.status == 0) {
          //
        }
        // Обновление данный прошло успешно
        if (res.status == 1) {
          $("#CardRegSuccess").removeClass("hidden");
          //
        }
      },
      error: function (jqXHR, exC1eption) {
        console.log("Error!");
        console.log(jqXHR);
        console.log(exC1eption);
      },
    });
  });

  /**
   * Динамическая отрисовка графика при изменений параметров
   */
  $(".form-task input").change(function (e) {
    draw();
  });

  var formTask = $(".form-task");
  if (formTask.length > 0) {
    draw();
  }
});

function draw() {
  var x0 = 250;
  var y0 = 400;
  var z0 = 0;

  var arc_radius_x = parseInt($(".form-task input[name=arc_radius_x]").val()) * 25;
  var arc_radius_y = parseInt($(".form-task input[name=arc_radius_y]").val()) * 25;
  var arc_z = z0 + 0 * 40;
  var arc_y = x0 + parseInt($(".form-task input[name=arc_y]").val()) * 25;
  var arc_x = y0 + parseInt($(".form-task input[name=arc_x]").val()) * 25;
  var arc_h = -(parseInt($(".form-task input[name=arc_h]").val()) * 100);




  var zA = 0;
  var xA = arc_y - arc_radius_y;
  var yA = arc_x

  var zB = 0;
  var xB = xA;
  var yB = yA + arc_h;

  var zC = 0;
  var xC = arc_y + arc_radius_y;
  var yC = arc_x

  var zD = 0;
  var xD = xC;
  var yD = yA + arc_h;


  var step = 25;

  // Прямая L1L2
  var zL1 = z0 + parseInt($("input[name=zl1]").val()) * 25;
  var xL1 = zL1 + x0 + parseInt($("input[name=xl1]").val()) * step;
  var yL1 = -zL1 + y0 - parseInt($("input[name=yl1]").val()) * step;
  var zL2 = z0 + parseInt($("input[name=zl2]").val()) * 25;
  var xL2 = zL2 + x0 + parseInt($("input[name=xl2]").val()) * step;
  var yL2 = -zL2 + y0 - parseInt($("input[name=yl2]").val()) * step;
  // Прямая N1N2
  var zN1 = z0 + parseInt($("input[name=zn1]").val()) * 25;
  var xN1 = zN1 + x0 + parseInt($("input[name=xn1]").val()) * step;
  var yN1 = -zN1 + y0 - parseInt($("input[name=yn1]").val()) * step;
  var zN2 = z0 + parseInt($("input[name=zn2]").val()) * 25;
  var xN2 = zN2 + x0 + parseInt($("input[name=xn2]").val()) * step;
  var yN2 = -zN2 + y0 - parseInt($("input[name=yn2]").val()) * step;
  // Прямая M1M2
  var zM1 = z0 + parseInt($("input[name=zm1]").val()) * 25;
  var xM1 = zM1 + x0 + parseInt($("input[name=xm1]").val()) * step;
  var yM1 = -zM1 + y0 - parseInt($("input[name=ym1]").val()) * step;
  var zM2 = z0 + parseInt($("input[name=zm2]").val()) * 25;
  var xM2 = zM2 + x0 + parseInt($("input[name=xm2]").val()) * step;
  var yM2 = -zM2 + y0 - parseInt($("input[name=ym2]").val()) * step;
  // Прямая K1K2
  var zK1 = z0 + parseInt($("input[name=zk1]").val()) * 25;
  var xK1 = zK1 + x0 + parseInt($("input[name=xk1]").val()) * step;
  var yK1 = -zK1 + y0 - parseInt($("input[name=yk1]").val()) * step;
  var zK2 = z0 + parseInt($("input[name=zk2]").val()) * 25;
  var xK2 = zK2 + x0 + parseInt($("input[name=xk2]").val()) * step;
  var yK2 = -zK2 + y0 - parseInt($("input[name=yk2]").val()) * step;
  // Прямая J1J2
  var zJ1 = z0 + parseInt($("input[name=zj1]").val()) * 25;
  var xJ1 = zJ1 + x0 + parseInt($("input[name=xj1]").val()) * step;
  var yJ1 = -zJ1 + y0 - parseInt($("input[name=yj1]").val()) * step;
  var zJ2 = z0 + parseInt($("input[name=zj2]").val()) * 25;
  var xJ2 = zJ2 + x0 + parseInt($("input[name=xj2]").val()) * step;
  var yJ2 = -zJ2 + y0 - parseInt($("input[name=yj2]").val()) * step;



  var canvas = document.getElementById("canvas");
  if (canvas.getContext) {
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.font = "12pt Arial";

    // Отрисовка направляющих
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.strokeStyle = "green";
    ctx.moveTo(x0, 0);
    ctx.lineTo(x0, 500);
    ctx.stroke();

    ctx.fillStyle = "green";
    ctx.fillText("0x", 10, y0 + 20);
    ctx.beginPath();
    ctx.strokeStyle = "red";
    ctx.moveTo(0, y0);
    ctx.lineTo(500, y0);
    ctx.stroke();

    ctx.fillStyle = "red";
    ctx.fillText("0y", x0 + 10, 20);

    ctx.beginPath();
    ctx.strokeStyle = "blue";
    ctx.moveTo(x0 - 500, y0 + 500);
    ctx.lineTo(x0 + 500, y0 - 500);
    ctx.stroke();

    ctx.fillStyle = "#00F";
    // Отрисовка фигуры
    ctx.lineWidth = 3;
    ctx.strokeStyle = "black";

    ctx.beginPath();
    ctx.ellipse(arc_y, arc_x, arc_radius_x, arc_radius_y, Math.PI / 2, 0, 2 * Math.PI);
    ctx.stroke();

    ctx.beginPath();
    ctx.ellipse(arc_y, arc_x + arc_h, arc_radius_x, arc_radius_y, Math.PI / 2, 0, 2 * Math.PI);
    ctx.stroke();


    ctx.moveTo(xA, yA);
    ctx.lineTo(xB, yB);
    ctx.stroke();
    ctx.moveTo(xC, yC);
    ctx.lineTo(xD, yD);
    ctx.stroke();

    // Отрисовка фигуры

    ctx.fillText("A", xA - 20, yA - 5);
    ctx.fillText("B", xB - 20, yB - 5);
    ctx.fillText("C", xC + 20, yC - 5);
    ctx.fillText("D", xD + 20, yD - 5);
    // Отрисовка прямых
    ctx.beginPath();
    ctx.lineWidth = 5;
    ctx.fillStyle = "red";
    ctx.strokeStyle = "red";
    // Прямая L1L2
    ctx.moveTo(xL1, yL1);
    ctx.lineTo(xL2, yL2);
    ctx.stroke();
    // Прямая N1N2
    ctx.moveTo(xN1, yN1);
    ctx.lineTo(xN2, yN2);
    ctx.stroke();
    // Прямая M1M2
    ctx.moveTo(xM1, yM1);
    ctx.lineTo(xM2, yM2);
    ctx.stroke();
    // Прямая K1K2
    ctx.moveTo(xK1, yK1);
    ctx.lineTo(xK2, yK2);
    ctx.stroke();
    // Прямая J1J2
    ctx.moveTo(xJ1, yJ1);
    ctx.lineTo(xJ2, yJ2);
    ctx.stroke();

    /*
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.strokeStyle = "black";
    ctx.moveTo(xA, yA);
    ctx.lineTo(xB1, yB1);
    ctx.moveTo(xB1, yB1);
    ctx.lineTo(xD1, yD1);
    ctx.moveTo(xD1, yD1);
    ctx.lineTo(xC1, yC1);
    ctx.moveTo(xC1, yC1);
    ctx.lineTo(xA, yA);
    ctx.moveTo(xA2, yA2);
    ctx.lineTo(xB2, yB2);
    ctx.moveTo(xB2, yB2);
    ctx.lineTo(xD2, yD2);
    ctx.moveTo(xD2, yD2);
    ctx.lineTo(xC2, yC2);
    ctx.moveTo(xC2, yC2);
    ctx.lineTo(xA2, yA2);
    ctx.moveTo(xA, yA);
    ctx.lineTo(xA2, yA2);
    ctx.moveTo(xB1, yB1);
    ctx.lineTo(xB2, yB2);
    ctx.moveTo(xC1, yC1);
    ctx.lineTo(xC2, yC2);
    ctx.moveTo(xD1, yD1);
    ctx.lineTo(xD2, yD2);
    ctx.stroke();
    ctx.fillText("A", xA - 20, yA - 5);
    ctx.fillText("B1", xB1 - 20, yB1 - 5);
    ctx.fillText("C1", xC1 - 20, yC1 - 5);
    ctx.fillText("D1", xD1 - 20, yD1 - 5);
    ctx.fillText("A2", xA2 - 20, yA2 - 5);
    ctx.fillText("B2", xB2 - 20, yB2 - 5);
    ctx.fillText("C2", xC2 - 20, yC2 - 5);
    ctx.fillText("D2", xD2 - 20, yD2 - 5);
    */

    // Отрисовка прямых
    /*
    ctx.beginPath();
    ctx.lineWidth = 5;
    ctx.fillStyle = "red";
    ctx.strokeStyle = "red";
    ctx.moveTo(xL1, yL1);
    ctx.lineTo(xL2, yL2);
    ctx.moveTo(xL3, yL3);
    ctx.lineTo(xL4, yL4);
    ctx.stroke();
    ctx.fillText("L1", xL1 + 5, yL1 + 20);
    ctx.fillText("L2", xL2 + 5, yL2 + 20);
    ctx.fillText("L3", xL3 + 5, yL3 + 20);
    ctx.fillText("L4", xL4 + 5, yL4 + 20);
    */
  }
}
