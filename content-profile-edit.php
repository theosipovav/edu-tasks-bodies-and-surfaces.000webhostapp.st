<div class="row">
    <div class="col-12">
        <?php
        $user = dbGetUserForId($_SESSION['user']['id']);
        ?>

        <form id="FormUpdateUser" class="d-flex flex-column bg-white p-3" action="ajax.php?f=ajaxUpdateUser" method="POST">
            <div id="CardRegError" class="card text-secondary bg-danger mt-3 mb-2 hidden">
                <div class="card-header">Внимание!</div>
                <div class="card-body">
                    <p class="card-text text-secondary">...</p>
                </div>
            </div>
            <div id="CardRegSuccess" class="card text-secondary bg-success mt-3 mb-2 hidden">
                <div class="card-header">Внимание!</div>
                <div class="card-body">
                    <p class="card-text text-secondary">Данные обновлены</p>
                </div>
            </div>
            <p class="h4 mb-2">Редактирование профиля <span><?= $use->name_first ?></span> <span><?= $user->name_last ?></span> </p>
            <input type="hidden" name="id" value="<?= $user->id ?>">
            <!-- Имя и фамилия -->
            <div class="form-row mb-2">
                <div class="col">
                    <input type="text" name="name_first" id="nameFirst" class="form-control mb-2" placeholder="Имя" value="<?= $user->name_first ?>" required>
                </div>
                <div class="col">
                    <input type="text" name="name_last" id="nameLast" class="form-control mb-2" placeholder="Фамилия" value="<?= $user->name_last ?>" required>
                </div>
            </div>
            <!-- Электронная почта -->
            <input type="email" id="inputMail" name="email" class="form-control mb-2" placeholder="Электронная почта" value="<?= $user->email ?>" required>
            <!-- Пароль -->
            <input type="password" id="inputPassword" name="password" class="form-control mb-2" placeholder="Пароль" aria-describedby="inputPasswordHelpBlock" value="<?= $user->password ?>" required>
            <!-- Повторите пароль -->
            <input type="password" id="inputRepeatPassword" name="password-repeat" class="form-control mb-2" placeholder="Повторите пароль" value="<?= $user->password ?>" required>
            <small id="inputPasswordHelpBlock" class="form-text text-muted mb-2">
                Минимальный размер пароля - 6 символов
            </small>
            <!-- Роль -->
            <div class="d-flex flex-column mb-2">
                <input type="tel" id="InputPhone" name="phone" class="form-control mb-2" placeholder="Номер телефона" aria-describedby="InputPhoneHelpBlock" value="<?= $user->phone ?>">
                <small id="InputPhoneHelpBlock" class="form-text text-muted text-center mb-2">
                    Необязательное
                </small>
                <div class="custom-control custom-radio">
                    <?php if ($user->role == 1) : ?>
                        <input type="radio" class="custom-control-input" id="radioTeacher" name="role" checked value="1" required>
                    <?php else : ?>
                        <input type="radio" class="custom-control-input" id="radioTeacher" name="role" value="1" required>
                    <?php endif ?>
                    <label class="custom-control-label w-150" for="radioTeacher">Преподаватель</label>
                </div>
                <div class="custom-control custom-radio">
                    <?php if ($user->role == 1) : ?>
                        <input type="radio" class="custom-control-input" id="radioStudent" name="role" value="2" required>
                    <?php else : ?>
                        <input type="radio" class="custom-control-input" id="radioStudent" name="role" value="2" checked required>
                    <?php endif ?>
                    <label class="custom-control-label w-150" for="radioStudent">Студент</label>
                </div>

            </div>
            <!-- Ваш преподаватель -->
            <?php if ($user->role == 1) : ?>
                <div id="ContainerSelectTeachers" class="row anim-300 disabled-hidden">
                <?php else : ?>
                    <div id="ContainerSelectTeachers" class="row anim-300 ">
                    <?php endif ?>

                    <label for="selectTeachers" class="col-5 col-form-label">Ваш преподаватель</label>
                    <div class="col-7">
                        <select name="select_teachers" id="selectTeachers" class="form-control mb-2">
                            <option value="-1" disabled>Выберите преподавателя</option>
                            <?php foreach (getTeachers() as $key => $teacher) : ?>
                                <option value="<?= $teacher["id"] ?>"><?= $teacher["username"] ?></option>
                            <?php endforeach ?>

                        </select>
                    </div>
                    </div>

                    <button class="btn btn-primary my-4 btn-block" type="submit">Сохранить</button>
                    <div id="CardRegError" class="card text-secondary bg-danger mt-3 anim-300 hidden">
                        <div class="card-header">Внимание!</div>
                        <div class="card-body">
                            <p class="card-text text-secondary">...</p>
                        </div>
                    </div>
        </form>
    </div>
</div>