<?php


$task = [];
if (hasTeacher()) {
    include_once 'content-tasks-teacher.php';
} else {
    include_once 'content-tasks-student.php';
}
