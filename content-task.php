<?php
$task = dbGetTask($_GET['id']);
//$nextTask = getNextTask($task->id);
$nextTask = false;
$answer = getAnswerForTask($_SESSION['user']['id'], $task->id);


?>
<div class="bg-light pt-3 pb-3">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2>Задание №<?= $task->id ?></h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12">
                <h3>Текст задания:</h3>
            </div>
            <div class="col-12">
                <p class="brown-text">
                    <?= $task->text ?>
                </p>
            </div>
        </div>
        <hr>






        <div id="FormCreateTask" class="form-task mr-auto ml-auto" action="/?r=tasks-add" method="POST">
            <div class="form-group row">
                <div class="col-12 d-flex justify-content-center align-content-center">
                    <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-2">
                    <div class="d-flex flex-column">
                        <div>
                            <h5 class="h5">Основание</h3>
                        </div>
                        <label class="form-control-label">0x
                            <input type="number" name="arc_y" value="<?= $task->arc_y ?>" class="col-sm-3 form-control">
                        </label>
                        <label class="form-control-label">0y
                            <input type="number" name="arc_x" value="<?= $task->arc_x ?>" class="col-sm-3 form-control">
                        </label>
                        <label class="form-control-label">Радиус оси x эллипса
                            <input type="number" name="arc_radius_x" min="0" value="<?= $task->arc_radius_x ?>" class="col-sm-3 form-control">
                        </label>
                        <label class="form-control-label">Радиус оси y эллипса.
                            <input type="number" name="arc_radius_y" min="0" value="<?= $task->arc_radius_y ?>" class="col-sm-3 form-control">
                        </label>
                        <label class="form-control-label">Высота
                            <input type="number" name="arc_h" value="<?= $task->arc_h ?>" class="col-sm-3 form-control">
                        </label>
                    </div>
                </div>
                <div class="col-12 col-md-10">
                    <div class="row">
                        <div class="col-12 col-md-2"></div>
                        <div class="col-12 col-md-2">
                            <div>
                                <h5 class="h5">Прямая L1L2</h5>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="form-control-label">xL1<input type="number" name="xl1" value="<?= $task->xl1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yL1<input type="number" name="yl1" value="<?= $task->yl1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zL1<input type="number" name="zl1" value="<?= $task->zl1 ?>" class="col-sm-3 form-control"></label>

                                </div>
                                <div class="col-6">
                                    <label class="form-control-label">xL2<input type="number" name="xl2" value="<?= $task->xl2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yL2<input type="number" name="yl2" value="<?= $task->yl2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zL2<input type="number" name="zl2" value="<?= $task->zl2 ?>" class="col-sm-3 form-control"></label>

                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div>
                                <h5 class="h5">Прямая N1N2</h5>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="form-control-label">xN1<input type="number" name="xn1" value="<?= $task->xn1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yN1<input type="number" name="yn1" value="<?= $task->yn1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zN1<input type="number" name="zn1" value="<?= $task->zn1 ?>" class="col-sm-3 form-control"></label>
                                </div>
                                <div class="col-6">
                                    <label class="form-control-label">xN2<input type="number" name="xn2" value="<?= $task->xn2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yN2<input type="number" name="yn2" value="<?= $task->yn2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zN2<input type="number" name="zn2" value="<?= $task->zn2 ?>" class="col-sm-3 form-control"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div>
                                <h5 class="h5">Прямая M1M2</h5>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="form-control-label">xM1<input type="number" name="xm1" value="<?= $task->xm1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yM1<input type="number" name="ym1" value="<?= $task->ym1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zM1<input type="number" name="zm1" value="<?= $task->zm1 ?>" class="col-sm-3 form-control"></label>
                                </div>
                                <div class="col-6">

                                    <label class="form-control-label">xM2<input type="number" name="xm2" value="<?= $task->xm2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yM2<input type="number" name="ym2" value="<?= $task->ym2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zM2<input type="number" name="zm2" value="<?= $task->zm2 ?>" class="col-sm-3 form-control"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div>
                                <h5 class="h5">Прямая K1K2</h5>
                            </div>
                            <div class="row">
                                <div class="col-6">

                                    <label class="form-control-label">xK1<input type="number" name="xk1" value="<?= $task->xk1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yK1<input type="number" name="yk1" value="<?= $task->yk1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zK1<input type="number" name="zk1" value="<?= $task->zk1 ?>" class="col-sm-3 form-control"></label>
                                </div>
                                <div class="col-6">

                                    <label class="form-control-label">xK2<input type="number" name="xk2" value="<?= $task->xk2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yK2<input type="number" name="yk2" value="<?= $task->yk2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zK2<input type="number" name="zk2" value="<?= $task->zk2 ?>" class="col-sm-3 form-control"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div>
                                <h5 class="h5">Прямая J1J2</h5>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label class="form-control-label">xJ1<input type="number" name="xj1" value="<?= $task->xj1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yJ1<input type="number" name="yj1" value="<?= $task->yj1 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zJ1<input type="number" name="zj1" value="<?= $task->zj1 ?>" class="col-sm-3 form-control"></label>
                                </div>
                                <div class="col-6">
                                    <label class="form-control-label">xJ2<input type="number" name="xj2" value="<?= $task->xj2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">yJ2<input type="number" name="yj2" value="<?= $task->yj2 ?>" class="col-sm-3 form-control"></label>
                                    <label class="form-control-label">zJ2<input type="number" name="zj2" value="<?= $task->zj2 ?>" class="col-sm-3 form-control"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <form id="FormResult" class="container mt-3" action="<?= $_SERVER['REQUEST_URI'] ?>" style="max-width: 350px;" method="post">
                                        <div class="row">
                                            <?php if (hasTeacher()) : ?>
                                                <label class="col-12">Ответ:
                                                    <input type="text" name="result" value="<?= $task->result ?>" class="form-control"></label>
                                            <?php else : ?>
                                                <label for="inputPyramidH" class="col-12 col-sm-5 col-form-label">Ваш ответ:</label>
                                                <?php if (!$answer) : ?>
                                                    <input type="text" name="result" value="" placeholder="???" class="form-control col-12 col-sm-7">
                                                <?php else : ?>
                                                    <input type="text" name="result" value="<?= $answer->result ?>" class="form-control col-12 col-sm-7">
                                                <?php endif ?>
                                                <input type="hidden" name="task_id" value="<?= $task->id ?>">
                                                <input type="hidden" name="user_id" value="<?= $_SESSION['user']['id'] ?>">
                                            <?php endif ?>

                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="d-flex flex-column m-auto" style="max-width: 350px;">
                                        <?php if (!hasTeacher()) : ?>
                                            <input type="submit" form="FormResult" class="btn btn-primary mt-3" name="from-result" value="Отправить ответ">
                                        <?php endif ?>
                                        <a href="/?r=task&id=<?= getNextTask($task->id)->id ?>" type="button" class="btn btn-outline-primary mt-3">Следующий вопрос</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($messageError != '') : ?>
                <div class="card text-white bg-danger mt-3 mb-3">
                    <div class="card-header">Внимание!</div>
                    <div class="card-body">
                        <h5 class="card-title"><?= $messageError ?></h5>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($messageSuccess != '') : ?>
                <div class="card text-white bg-success mt-3 mb-3">
                    <div class="card-header">Внимание!</div>
                    <div class="card-body">
                        <p class="card-title"><?= $messageSuccess ?></p>
                    </div>
                </div>
            <?php endif ?>



        </div>