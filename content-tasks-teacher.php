<div class="row">
    <div class="col-12">
        <h2 class="h2 mb-3 text-white">Задания</h2>
    </div>

    <div class="col-12 mb-3">
        <div class="card bg-light pt-2 pb-2">
            <div class="row">
                <div class="col-12 col-md-6 text-center">
                    <a href="/?r=tasks&filter=1" class="btn btn-primary m-1">Ваши задания</a>
                </div>
                <div class="col-12 col-md-6 text-center">
                    <a href="/?r=tasks" class="btn btn-primary m-1">Все задания</a>
                </div>

            </div>
        </div>
    </div>

    <div class="col-12">
        <?php foreach (dbGetTasks() as $key => $task) : ?>
            <?php
            if (isset($_GET['filter'])) {
                if ($task->user_id != $_SESSION['user']['id']) {
                    continue;
                }
            }
            ?>

            <div class="card bg-light mb-3">
                <div class="card-header">Задание №<?= $task->id ?></div>
                <div class="card-body">
                    <p class="card-text"><?= $task->text ?></p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-12 col-md-6 d-flex align-items-center">
                            <div class="d-flex justify-content-center">
                                <span class="me-1">Автор: </span><span class="me-1"><?= dbGetUserForId($task->user_id)->name_last; ?></span> <span><?= dbGetUserForId($task->user_id)->name_first; ?></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 d-flex align-items-center">
                            <div class="d-flex justify-content-end flex-grow-1">
                                <button type="button" class="btn btn-danger btn-remove-task m-1" data-bs-toggle="modal" data-bs-target="#modalRemoveTask_<?= $task->id ?>" data-task-id="<?= $task->id ?>">
                                    Удалить
                                </button>
                                <a href="/?r=task&id=<?= $task->id ?>" class="btn btn-primary m-1">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalRemoveTask_<?= $task->id ?>" tabindex="-1" aria-labelledby="modalRemoveTask_<?= $task->id ?>Label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalRemoveTask_<?= $task->id ?>Label">Удаление задания <?= $task->id ?> </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Вы действительно хотите удалить задание?
                        </div>
                        <div class="modal-footer">
                            <form id="FormRemoveTask_<?= $task->id ?>" action="" method="post" style="display: none;">
                                <input type="hidden" name="task_id" value="<?= $task->id ?>">
                            </form>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                            <button type="submit" class="btn btn-danger btn-ok" name="form-remove-task" form="FormRemoveTask_<?= $task->id ?>">Удалить</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>


    </div>
</div>