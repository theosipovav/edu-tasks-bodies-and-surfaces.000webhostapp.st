<?php

global $messageError;
global $messageSuccess;


?>


<div class="row">
    <div class="col-12">
        <div class="bg-light p-3">
            <?php if ($_SESSION['user']['role'] == 1) : ?>
                <form id="FormCreateTask" class="form-task mr-auto ml-auto" action="/?r=tasks-add" method="POST">
                    <div class="d-flex flex-column align-items-center">
                        <img class="mb-4" src="/assets/img/tasks.png" alt="" width="150">
                        <h1 class="h1 mb-3 font-weight-normal text-center">Добавить новое задание</h1>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-md-6">
                            <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="d-flex flex-column">
                                <label for="textareaText" class="h5 col-form-label">Задание</label>
                                <textarea type="text" name="text" id="textareaText" class="form-control" rows="5" required>Прямоугольная трапеция с основаниями 5 см и 10 см и большой боковой стороной равной 13 см вращается вокруг меньшего основания. 
Найдите площадь поверхности тела вращения.</textarea>
                            </div>
                            <div class="d-flex flex-column">
                                <label for="textareaAnswer" class="h5 col-sm-4 col-form-label">Правильный ответ</label>
                                <input type="text" name="answer" id="textareaAnswer" class="form-control" value="540*pi см^2">
                            </div>
                            <div class="d-flex flex-column">
                                <label for="textareaDesc" class="h5 col-sm-4 col-form-label">Решение</label>
                                <textarea type="text" name="desc" id="textareaDesc" class="form-control" rows="5" required>DC=5см, AD=10см, AB=13см
S(тела)=S(бок. кон.)+S(цил, 1 основание)
S(тела)=pi*r*l+2*pi*r*h+pi*r^2;
AK=AD-DC=5см
Из треугольника AKB - прямоугольного по теореме Пифагора
KB=12см-r
AB=1
h=AD=10см
S(тела)=pi*12*13+2*pi12*10+144*pi=540*pi см^2</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-md-2">
                            <div class="d-flex flex-column">
                                <div>
                                    <h5 class="h5">Основание</h3>
                                </div>
                                <label class="form-control-label">0x
                                    <input type="number" name="arc_y" value="0" class="col-sm-3 form-control">
                                </label>
                                <label class="form-control-label">0y
                                    <input type="number" name="arc_x" value="0" class="col-sm-3 form-control">
                                </label>
                                <label class="form-control-label">Радиус оси x эллипса
                                    <input type="number" name="arc_radius_x" min="0" value="1" class="col-sm-3 form-control">
                                </label>
                                <label class="form-control-label">Радиус оси y эллипса.
                                    <input type="number" name="arc_radius_y" min="0" value="5" class="col-sm-3 form-control">
                                </label>
                                <label class="form-control-label">Высота
                                    <input type="number" name="arc_h" value="3" class="col-sm-3 form-control">
                                </label>
                            </div>
                        </div>
                        <div class="col-12 col-md-10">
                            <div class="row">
                                <div class="col-12 col-md-2"></div>
                                <div class="col-12 col-md-2">
                                    <div>
                                        <h5 class="h5">Прямая L1L2</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="form-control-label">xL1<input type="number" name="xl1" value="0" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yL1<input type="number" name="yl1" value="0" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zL1<input type="number" name="zl1" value="0" class="col-sm-3 form-control"></label>

                                        </div>
                                        <div class="col-6">
                                            <label class="form-control-label">xL2<input type="number" name="xl2" value="0" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yL2<input type="number" name="yl2" value="12" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zL2<input type="number" name="zl2" value="0" class="col-sm-3 form-control"></label>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div>
                                        <h5 class="h5">Прямая N1N2</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="form-control-label">xN1<input type="number" name="xn1" value="-5" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yN1<input type="number" name="yn1" value="12" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zN1<input type="number" name="zn1" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                        <div class="col-6">
                                            <label class="form-control-label">xN2<input type="number" name="xn2" value="0" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yN2<input type="number" name="yn2" value="7" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zN2<input type="number" name="zn2" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div>
                                        <h5 class="h5">Прямая M1M2</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <label class="form-control-label">xM1<input type="number" name="xm1" value="5" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yM1<input type="number" name="ym1" value="12" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zM1<input type="number" name="zm1" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                        <div class="col-6">

                                            <label class="form-control-label">xM2<input type="number" name="xm2" value="0" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yM2<input type="number" name="ym2" value="7" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zM2<input type="number" name="zm2" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div>
                                        <h5 class="h5">Прямая K1K2</h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">

                                            <label class="form-control-label">xK1<input type="number" name="xk1" value="-5" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yK1<input type="number" name="yk1" value="0" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zK1<input type="number" name="zk1" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                        <div class="col-6">

                                            <label class="form-control-label">xK2<input type="number" name="xk2" value="5" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yK2<input type="number" name="yk2" value="0" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zK2<input type="number" name="zk2" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <div>
                                        <h5 class="h5">Прямая J1J2</h5>
                                    </div>
                                    <div class="row">

                                        <div class="col-6">
                                            <label class="form-control-label">xJ1<input type="number" name="xj1" value="-5" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yJ1<input type="number" name="yj1" value="12" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zJ1<input type="number" name="zj1" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                        <div class="col-6">
                                            <label class="form-control-label">xJ2<input type="number" name="xj2" value="5" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">yJ2<input type="number" name="yj2" value="12" class="col-sm-3 form-control"></label>
                                            <label class="form-control-label">zJ2<input type="number" name="zj2" value="0" class="col-sm-3 form-control"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <input type="hidden" name="user_id" value="<?= $_SESSION['user']['id'] ?>">
                                <div class="col-12 d-flex flex-column mt-3" style="max-width: 300px;">
                                    <button type="reset" class="btn btn-outline-primary m-1">Сбросить все</button>
                                    <button type="submit" name="form-create-task" class="btn btn-primary m-1">Добавить задание</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($messageError != '') : ?>
                        <div class="card text-white bg-danger mt-3 mb-3">
                            <div class="card-header">Внимание!</div>
                            <div class="card-body">
                                <h5 class="card-title"><?= $messageError ?></h5>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if ($messageSuccess != '') : ?>
                        <div class="card text-white bg-success mt-3 mb-3">
                            <div class="card-header">Внимание!</div>
                            <div class="card-body">
                                <p class="card-title"><?= $messageSuccess ?></p>
                            </div>
                        </div>
                    <?php endif ?>



                </form>
            <?php else : ?>
                <div class="card text-white bg-warning mb-3" style="margin: auto;">
                    <div class="card-header">Внимание</div>
                    <div class="card-body">
                        <h5 class="card-title">Добавить новое задание может только преподаватель</h5>
                    </div>
                <?php endif ?>

                </div>
        </div>
    </div>