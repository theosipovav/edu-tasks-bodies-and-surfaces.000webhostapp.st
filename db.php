<?php
define("db_host", "localhost");
define("db_name", "id15715156_bodies_surfaces");
define("db_user", "id15715156_admin");
define("db_password", "T}L=\}/_NMH=+6Ke");




$mysqli = new mysqli(db_host, db_user, db_password, db_name);
if ($mysqli->connect_errno) {
    $error = "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    writeLog($error);
    exit();
}

/**
 * Получить последнюю ошибку с БД
 */
function dbLastError()
{
    global $mysqli;
    return $mysqli->error;
}
/**
 * 
 */
function writeLog($message)
{
    try {
        $filename = 'log_' . date('YmdHis') . '.txt';
        $fp = fopen("logs//" . $filename, 'w');
        fwrite($fp, $message);
        fclose($fp);
    } catch (Exception $e) {
        echo "Критическая ошибка!<br>";
        echo $e;
        die();
    }
}

/**
 * Создание нового пользователя 
 */
function dbCreateUser($email, $password, $nameFirst, $nameLast, $role, $phone)
{
    global $mysqli;

    $query = "INSERT INTO `users` (`email`, `password`, `name_first`, `role`, `created_dt`, `update_dt`, `name_last`, `phone`) VALUES ('$email', '$password', '$nameFirst', '$role', current_timestamp(), current_timestamp(), '$nameLast', '$phone')";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        writeLog($mysqli->error);
        return false;
    }
}

/**
 * Поиск пользователя
 */
function dbFindUser($email, $password = '')
{
    global $mysqli;
    $user = false;
    if ($password == '') {
        $query = "SELECT `id`, `email`, `name_first`, `name_last`, `role`, `phone`, `created_dt` FROM `users` WHERE `email` = '$email'";
    } else {
        $query = "SELECT `id`, `email`, `name_first`, `name_last`, `role`, `phone`, `created_dt` FROM `users` WHERE `email` = '$email' and `password` = '$password'";
    }
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $user = [];
            $user["id"] = $row[0];
            $user["email"] = $row[1];
            $user["name_first"] = $row[2];
            $user["name_last"] = $row[3];
            $user["role"] = $row[4];
            $user["phone"] = $row[5];
            $user["created_dt"] = $row[6];
            break;
        }
        $res->close();
    }
    return $user;
}

/**
 * Получить имя роли
 */
function dbGetNameRole($id)
{
    global $mysqli;
    $nameRole = false;
    $query = "SELECT * FROM `roles` WHERE `id` = $id";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $nameRole = $row[1];
            break;
        }
        $res->close();
    }
    return $nameRole;
}

/**
 * База данных. Получить пользователя по его идентификатору
 */
function dbGetUserForId($id)
{
    global $mysqli;
    $user = false;
    $query = "SELECT * FROM `users` WHERE `id` = '$id'";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $user = $obj;
            break;
        }
        $res->close();
    }
    return $user;
}

/**
 * База данных. Получить список учеников по идентификатору учителя
 */
function dbGetGroupsForChildId($id)
{
    global $mysqli;
    $groups = [];
    $query = "select id, parent_id, child_id from `group` where `group`.child_id = '$id';";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $groups[] = $obj;
            break;
        }
        $res->close();
    }
    return $groups;
}

/**
 * База данных. Получить список преподавателей учеников по идентификатору ученика
 */
function dbGetGroupsForParentId($id)
{
    global $mysqli;
    $groups = [];
    $query = "select id, parent_id, child_id from `group` where `group`.parent_id = '$id';";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $group = [];
            $group['id'] = $obj->id;
            $group['parent_id'] = $obj->parent_id;
            $group['child_id'] = $obj->child_id;
            $groups[] = $group;
            break;
        }
        $res->close();
    }
    return $groups;
}

/**
 * Обновление записи о пользователе
 */
function dbUpdateUser($id, $email, $password, $nameFirst, $nameLast, $role, $phone)
{
    global $mysqli;
    $updateDate = date("Y-m-d H:i:s");
    $query = "UPDATE `users` ";
    $query .= "SET `email` = '$email', `password` = '$password', `name_first` = '$nameFirst', `role` = '$role', `update_dt` = '$updateDate', `name_last` = '$nameLast', `phone` = '$phone' ";
    $query .= "WHERE `users`.`id` = $id";

    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        writeLog($mysqli->error);
        return false;
    }
}

/**
 * База данных. Прикрепить ученика к преподавателю.
 */
function dbCreateGroup($parent_id, $child_id)
{
    global $mysqli;
    $query = "INSERT INTO `group` (`parent_id`, `child_id`) VALUES ('$parent_id', '$child_id')";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        writeLog($mysqli->error);
        return false;
    }
}
/**
 * База данных. Удалить ученика у преподавателей.
 */
function dbRemoveGroup($child_id)
{
    global $mysqli;
    $query = "SELECT * FROM `group` WHERE `child_id` like '$child_id'";
    if ($res = $mysqli->query($query)) {
        if ($res->field_count == 0) {
            return true;
        }
    } else {
        writeLog($mysqli->error);
        return false;
    }

    $query = "DELETE FROM `group` WHERE `group`.`child_id` = '$child_id'";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        writeLog($mysqli->error);
        return false;
    }
}


/**
 * Создание ответа
 */
function dbCreateOrUpdateAnswer($user_id, $task_id, $result)
{
    global $mysqli;
    session_start();
    if (isset($_SESSION['user']['id'])) {
        $answer = false;
        $query = "SELECT id FROM `answers` where user_id = $user_id and task_id = $task_id";
        if ($res = $mysqli->query($query)) {
            while ($obj = $res->fetch_object()) {
                $answer = $obj;
                break;
            }
            $res->close();
        }
        if ($answer != false) {
            // Обновление существующего ответа
            $query = "UPDATE `answers` SET `result` = '$result' WHERE `answers`.`id` = " . $answer->id;
            if ($mysqli->query($query) === TRUE) {
                return true;
            } else {
                $fp = fopen('log.txt', 'w');
                fwrite($fp, 'Не удалось обновить существующую запись.');
                fclose($fp);
                return false;
            }
        } else {
            // Создание нового ответа
            $query = "INSERT INTO `answers` (`user_id`, `task_id`, `result`) VALUES ('$user_id', $task_id, '$result')";
            if ($mysqli->query($query) === TRUE) {
                return true;
            } else {
                $fp = fopen('log.txt', 'w');
                fwrite($fp, 'Не удалось создать запись нового ответа');
                fclose($fp);
                return false;
            }
        }
    } else {
        $fp = fopen('log.txt', 'w');
        fwrite($fp, 'Пользователь не авторизован');
        return false;
    }
}
/**
 * Получить все задания 
 */
function dbGetTasks()
{
    global $mysqli;
    $tasks = [];
    $query = "SELECT * FROM `tasks`";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $tasks[] = $obj;
        }
        $res->close();
    }
    return $tasks;
}
/**
 * Получить задание по его идентификатору
 * Результат: ARRAY (id, text, decision, answer_v, answer_h, answer_a, answer_b)
 */
function dbGetTask($id)
{
    global $mysqli;
    $task = false;
    $query = "SELECT * FROM `tasks` WHERE `id` = $id";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $task = $obj;
            break;
        }
        $res->close();
    }
    return $task;
}
/**
 * Получить ответ по идентификатору пользователя и задачи
 * Результат: ARRAY(id, user_id, task_id, h, a, v)
 */
function getAnswerForTask($userId, $taskId)
{
    global $mysqli;
    $answer = false;
    $query = "SELECT * FROM `answers` WHERE user_id = $userId and task_id = $taskId";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $answer = $obj;
            break;
        }
        $res->close();
    }
    return $answer;
}

/**
 * Получить все задания для студента 
 */
function dbGetTasksForStudent($user_id)
{
    global $mysqli;
    $tasks = [];
    $teachers = dbGetIndividualTeachers($user_id);
    foreach ($teachers as $key => $user) {
        $id  = $user->id;
        $query = "select * from tasks where user_id = '$id'";
        if ($res = $mysqli->query($query)) {
            while ($obj = $res->fetch_object()) {
                $tasks[] = $obj;
            }
            $res->close();
        }
    }
    return $tasks;
}

/**
 * База данных. Получить индивидуального преподавателя по идентификатору ученика
 */
function dbGetIndividualTeachers($id)
{
    $groups = dbGetGroupsForChildId($id);
    $users = [];
    foreach ($groups as $g) {
        $user = dbGetUserForId($g->parent_id);
        $users[] = $user;
    }

    return $users;
}
/**
 * База данных. Получить учеников по идентификатору учителя
 */
function dbGetIndividualStudent($id)
{
    $groups = dbGetGroupsForParentId($id);
    $users = [];
    foreach ($groups as $g) {
        $user = dbGetUserForId($g->child_id);
        $users[] = $user;
    }

    return $users;
}

/**
 * База данных. Получить всех пользователей
 */
function dbGetUsers($roleId = null)
{
    global $mysqli;
    $users = [];

    if ($roleId == null) {
        $query = "SELECT * FROM `users`";
    } else {
        $query = "SELECT * FROM `users` WHERE `role` = '$roleId'";
    }
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $user = [];
            $user["id"] = $row[0];
            $user["mail"] = $row[1];
            $user["password"] = $row[2];
            $user["username"] = $row[3];
            $user["role"] = $row[4];
            $user["created_dt"] = $row[5];
            $users[] = $user;
        }
        $res->close();
    }
    return $users;
}

/**
 * Получить все ответы студентов
 */
function getAnswers($userId = null)
{
    global $mysqli;
    $answers = [];

    if ($userId == null) {
        $query = "SELECT * FROM `answers`";
    } else {
        $query = "SELECT * FROM `answers` WHERE `answers`.`user_id` = '$userId'";
    }

    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $answers[] = $obj;
        }
        $res->close();
    }
    return $answers;
}

/**
 * Получить отображаемое имя пользователя
 */
function dbGetNameUser($id)
{

    global $mysqli;
    $name = false;
    $query = "SELECT * FROM `users` WHERE `id` = $id";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $name = $obj->name_last . " " . $obj->name_first;
            break;
        }
        $res->close();
    }
    return $name;
}

/**
 * Получить ответ по его идентификатору
 * Результат: ARRAY(id, user_id, task_id, h, a, v)
 */
function dbGetResult($id)
{
    global $mysqli;
    $answer = false;
    $query = "SELECT * FROM `answers` WHERE `id` = '$id'";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $answer = $obj;
            break;
        }
        $res->close();
    }
    return $answer;
}

/**
 * База данных. Получить всех преподавателей
 */
function getTeachers()
{
    $users = [];
    foreach (dbGetUsers(1) as $user) {
        $users[] = $user;
    }
    return $users;
}

/**
 * База данных. Создание нового задания.
 */
function dbAddTask($data)
{
    $text = $data["text"];
    $answer = $data["answer"];
    $desc = $data["desc"];
    $arc_y = $data["arc_y"];
    $arc_x = $data["arc_x"];
    $arc_radius_x = $data["arc_radius_x"];
    $arc_radius_y = $data["arc_radius_y"];
    $arc_h = $data["arc_h"];
    $xl1 = $data["xl1"];
    $yl1 = $data["yl1"];
    $zl1 = $data["zl1"];
    $xl2 = $data["xl2"];
    $yl2 = $data["yl2"];
    $zl2 = $data["zl2"];
    $xn1 = $data["xn1"];
    $yn1 = $data["yn1"];
    $zn1 = $data["zn1"];
    $xn2 = $data["xn2"];
    $yn2 = $data["yn2"];
    $zn2 = $data["zn2"];

    $xm1 = $data["xm1"];
    $ym1 = $data["ym1"];
    $zm1 = $data["zm1"];
    $xm2 = $data["xm2"];
    $ym2 = $data["ym2"];
    $zm2 = $data["zm2"];

    $xk1 = $data["xk1"];
    $yk1 = $data["yk1"];
    $zk1 = $data["zk1"];
    $xk2 = $data["xk2"];
    $yk2 = $data["yk2"];
    $zk2 = $data["zk2"];

    $xj1 = $data["xj1"];
    $yj1 = $data["yj1"];
    $zj1 = $data["zj1"];
    $xj2 = $data["xj2"];
    $yj2 = $data["yj2"];
    $zj2 = $data["zj2"];

    $user_id = $data["user_id"];

    global $mysqli;
    $query = "INSERT INTO `tasks` ";
    $query .= "(`text`, `answer`, `user_id`, `created_dt`, `update_dt`, `desc`, `arc_x`, `arc_y`, `arc_radius_x`, `arc_radius_y`, `arc_h`, `xl1`, `yl1`, `zl1`, `xl2`, `yl2`, `zl2`, `xn1`, `yn1`, `zn1`, `xn2`, `yn2`, `zn2`, `xm1`, `ym1`, `zm1`, `xm2`, `ym2`, `zm2`, `xk1`, `yk1`, `zk1`, `xk2`, `yk2`, `zk2`, `xj1`, `yj1`, `zj1`, `xj2`, `yj2`, `zj2`) ";
    $query .= "VALUES ( '$text', '$answer', '$user_id', current_timestamp(), current_timestamp(), '$desc', '$arc_x', '$arc_y', '$arc_radius_x', '$arc_radius_y', '$arc_h', '$xl1', '$yl1', '$zl1', '$xl2', '$yl2', '$zl2', '$xn1', '$yn1', '$zn1', '$xn2', '$yn2', '$zn2', '$xm1', '$ym1', '$zm1', '$xm2', '$ym2', '$zm2', '$xk1', '$yk1', '$zk1', '$xk2', '$yk2', '$zk2', '$xj1', '$yj1', '$zj1', '$xj2', '$yj2', '$zj2');";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        $filename = 'log_' . date('YmdHis') . '.txt';
        $fp = fopen($filename, 'w');
        fwrite($fp, dbLastError());
        fclose($fp);
        return false;
    }
}

/**
 * База данных. Удаление задания по его идентификатору.
 */
function dbRemoveTask($id)
{
    global $mysqli;
    $query = "DELETE FROM `tasks` WHERE id = $id;";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        $filename = 'log_' . date('YmdHis') . '.txt';
        $fp = fopen($filename, 'w');
        fwrite($fp, dbLastError());
        fclose($fp);
        return false;
    }
}
