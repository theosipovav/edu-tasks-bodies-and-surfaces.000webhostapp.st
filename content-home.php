<?php

/**
 * Стартовая страница
 * 
 * 
 */



?>



<div class="row">
    <div class="col-12 col-lg-6">
        <?php include_once "module-hello.php" ?>
    </div>
    <div class="col-12 col-lg-6">
        <?php if ($isAuth) : ?>
            <?php if (hasTeacher()) : ?>
                <?php include_once "module-profile-teacher.php" ?>
            <?php else : ?>
                <?php include_once "module-profile-student.php" ?>
            <?php endif ?>
        <?php else : ?>
            <?php include_once "module-login.php" ?>
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <h2 class="h2 text-light">Пример задания</h2>
    </div>
    <div class="col-12 d-flex justify-content-center align-content-center">
        <img src="assets/img/example.jpg" class="img-fluid mt-3 mb-3">

    </div>
</div>