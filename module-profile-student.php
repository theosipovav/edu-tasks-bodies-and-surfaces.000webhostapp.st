<?php
$user = $_SESSION['user'];
?>
<div class="card card-cascade">
    <div class="view view-cascade gradient-card-header blue-gradient text-center">
        <h2 class="card-header-title mb-3 text-secondary">
            <span><?= $user['name_last'] ?></span>
            <br>
            <span><?= $user['name_first'] ?></span>
        </h2>
        <p class="card-header-subtitle mb-1"><?= dbGetNameRole($user['role']) ?></p>
    </div>
    <div class="card-body card-body-cascade text-center">

        <table class="table table-striped table-sm table-profile">
            <tbody>
                <tr>
                    <th scope="row">Электронный адрес</th>
                    <td><?= $user['email'] ?></td>
                </tr>
                <tr>
                    <th scope="row">Имя пользователя</th>
                    <td><?= $user['name_first'] ?></td>
                </tr>
                <tr>
                    <th scope="row">Фамилия пользователя</th>
                    <td><?= $user['name_last'] ?></td>
                </tr>
                <tr>
                    <th scope="row">Роль пользователя</th>
                    <td><?= dbGetNameRole($user['role']) ?></td>
                </tr>
                <tr>
                    <th scope="row">Дата регистрации</th>
                    <td><?= $user['created_dt'] ?></td>
                </tr>
                <tr>
                    <th scope="row">Номер мобильного телефона</th>
                    <td><?= $user['phone'] ?></td>
                </tr>
                <tr>
                    <th scope="row">Преподаватели</th>
                    <td>
                        <div class="d-flex flex-column">
                            <?php foreach (dbGetIndividualTeachers($user['id']) as $teacher) : ?>
                                <div><span><?= $teacher->name_last ?></span> <span><?= $teacher->name_first ?></span></div>
                            <?php endforeach ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <hr>

        <div class="d-flex flex-column">
            <a href="?r=profile-edit" type="submit" class="btn btn-primary mt-1" form="FormLogout">Редактировать</a>
            <a class="btn btn-primary mt-1" href="/?logout=1">Выйти</a>

        </div>
    </div>

</div>