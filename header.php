  <!--Main Navigation-->
  <header class="header">
      <nav class="navbar navbar-expand-lg navbar-dark">
          <div class="container">
              <a class="navbar-brand" href="/" title="Метод координат"><strong>Тела и их поверхности</strong></a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                      <li class="nav-item active">
                          <a class="nav-link" href="/">Главная <span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="?r=tasks">Задания</a>
                      </li>
                      <?php if (hasTeacher()) : ?>
                          <li class="nav-item">
                              <a class="nav-link" href="?r=tasks-add">Добавить новую задачу</a>
                          </li>
                      <?php endif ?>
                      <li class="nav-item">
                          <a class="nav-link" href="?r=results">Результат</a>
                      </li>
                  </ul>
              </div>
          </div>
      </nav>


  </header>
  <!--Main -->