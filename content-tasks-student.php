<div class="row ">
    <div class="col-12">
        <h2 class="h2 mb-3 text-white">Задания</h2>
    </div>
    <div class="col-12">

        <div id="tablePreview" class="bg-light pt-3 pb-3">
            <div class="row">
                <div class="col-12 col-md-1 text-center font-weight-bold">№</div>
                <div class="col-12 col-md-6 text-center font-weight-bold">Задание</div>
                <div class="col-12 col-md-3 text-center font-weight-bold">Автор</div>
                <div class="col-12 col-md-2 text-center font-weight-bold"></div>
            </div>

            <hr>
            <?php foreach (dbGetTasksForStudent($_SESSION['user']['id']) as $key => $task) : ?>
                <div class="row">
                    <div class="col-12 col-md-1 text-center"><?= $task->id ?></div>
                    <div class="col-12 col-md-6 text-center">
                        <p class="text-dark p-1 text-center">
                            <?= $task->text ?>
                        </p>
                    </div>
                    <div class="col-12 col-md-3">
                        <p class="text-dark p-1 text-center">
                            <?= dbGetUserForId($task->user_id)->name_last; ?>

                        </p>
                    </div>
                    <div class="col-12 col-md-2 text-center"><a href="/?r=task&id=<?= $task->id ?>" class="btn btn-primary m-1">Перейти</a></div>
                </div>
                <hr>
            <?php endforeach ?>
        </div>
    </div>
</div>