<?php
// Отключение вывода ошибок
error_reporting(1);

// Подключение набора функция по работе с базой данных
include_once 'db.php';

// Подключение набора функций по управлению содержимом на сайте
include_once 'functions.php';
?>
<!DOCTYPE html>
<html lang="en">
<?php
// Подключение HTTP заголовков
include_once 'head.php';
?>

<body>
  <?php
  // Шапка сайта
  include_once 'header.php';
  // Содержимое страницы
  include_once 'content.php';
  // Подвал сайта
  include_once 'footer.php';
  ?>
</body>

</html>