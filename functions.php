<?php

// Сессия
session_start();
// Глобальные переменные
$isAuth = false;
$messageError = "";
$messageSuccess = "";


function d($test)
{
    echo "<pre>" . print_r($test, true) . "</pre>";
    exit();
}



if (isset($_GET['logout'])) {
    logout();
}

if (isset($_SESSION['user'])) {
    $isAuth = true;
}



// Регистрация ответа
if (isset($_POST['from-result'])) {
    dbCreateOrUpdateAnswer($_POST['user_id'], $_POST['task_id'], $_POST['result']);
}
// Регистрация задания
if (isset($_POST['form-create-task'])) {


    if (dbAddTask($_POST)) {
        $messageSuccess = "Задание добавлено";
    } else {
        $messageError = "Не удалось добавить задание";
    }
}
// Удаления задания задания
if (isset($_POST['form-remove-task'])) {
    dbRemoveTask($_POST['task_id']);
}


/**
 * Авторизация пользователя в системе
 */
function login()
{
    global $isAuth;
    global $messageError;
    $post = $_POST;
    $user = dbFindUser($post["email"], $post["password"]);
    if (is_array($user)) {
        $_SESSION['user'] =  $user;
        $isAuth = true;
        return true;
    } else {
        return false;
        $messageError = "Пользователь не найден. Возможно введен неверный логин или пароль.";
    }
}

/**
 * Выход пользователя из системы
 */
function logout()
{

    $res = [];
    $res['status'] = 0;
    $res['data'] = [];
    session_start();
    $_SESSION = [];
    $res['status'] = 1;
}
/**
 * Проверка пользователя на роль учителя 
 */
function hasTeacher()
{
    session_start();
    if ($_SESSION['user']['role'] == 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * Проверка ответа
 */
function checkResultTask($id)
{
    $answer = dbGetResult($id);
    $task = dbGetTask($answer->task_id);
    if ($answer->result == $task->answer) {
        return true;
    } else {
        return false;
    }
}


function getNextTask($task_id)
{
    $tasks = dbGetTasks();
    $taskNext = $tasks[0];
    $isFlag = false;
    foreach ($tasks as $key => $task) {
        if ($isFlag) {
            $taskNext = $task;
            break;
        }
        if ($task->id == $task_id) {
            $isFlag = true;
        }
    }
    return $taskNext;
}


function getUrl()
{
    $url = $_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    $url = $url[0];
    return $url;
}
