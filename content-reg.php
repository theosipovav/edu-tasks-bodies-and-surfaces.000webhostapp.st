<div class="row">
    <div class="col-6">
        <div class="text-white text-center py-5 px-4">
            <div>
                <h2 class="card-title h1-responsive pt-3 mb-5 font-bold">Регистрация нового пользователя</h2>
                <p class="mx-5 mb-5">
                    После создания нового пользователя вам станет доступен основной функционал веб приложения!
                </p>
                <hr>
                <p>
                    Если у вас уже есть аккаунт, то вы можете войти в систему под своими учетными данными.
                </p>
                <a href="/" class="btn btn-light"><i class="fas fa-clone left"></i> Войти</a>
            </div>
        </div>
    </div>
    <div class="col-6">
        <form id="formReg" class="d-flex flex-column bg-white p-3" action="#!">
            <p class="h4 mb-4">Регистрация нового пользователя</p>
            <div class="form-row mb-4">
                <div class="col">
                    <input type="text" name="name_first" id="nameFirst" class="form-control mb-2" placeholder="Имя" required>
                </div>
                <div class="col">
                    <input type="text" name="name_last" id="nameLast" class="form-control mb-2" placeholder="Фамилия" required>
                </div>
            </div>
            <!-- Электронная почта -->
            <input type="email" id="inputMail" name="email" class="form-control mb-2" placeholder="Электронная почта" required>
            <!-- Пароль -->
            <input type="password" id="inputPassword" name="password" class="form-control mb-2" placeholder="Пароль" aria-describedby="inputPasswordHelpBlock" required>
            <!-- Повторите пароль -->
            <input type="password" id="inputRepeatPassword" name="password-repeat" class="form-control mb-2" placeholder="Повторите пароль" required>
            <small id="inputPasswordHelpBlock" class="form-text text-muted text-center mb-2">
                Минимальный размер пароля - 6 символов
            </small>
            <!-- Роль -->
            <div class="d-flex flex-column mb-2">
                <input type="text" id="InputPhone" name="phone" class="form-control mb-2" placeholder="Номер телефона (92158963256)" required>
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="radioTeacher" name="role" value="1" required>
                    <label class="custom-control-label w-150" for="radioTeacher">Преподаватель</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="radioStudent" name="role" value="2" checked required>
                    <label class="custom-control-label w-150" for="radioStudent">Студент</label>
                </div>

            </div>
            <!-- Ваш преподаватель -->
            <div id="ContainerSelectTeachers" class="row anim-300">
                <label for="selectTeachers" class="col-5 col-form-label">Ваш преподаватель</label>
                <div class="col-7">
                    <select name="select_teachers" id="selectTeachers" class="form-control">
                        <option value="-1" disabled>Выберите преподавателя</option>
                        <?php foreach (getTeachers() as $key => $user) : ?>
                            <option value="<?= $user["id"] ?>"><?= $user["username"] ?></option>
                        <?php endforeach ?>

                    </select>
                </div>
            </div>

            <button class="btn btn-primary my-4 btn-block" type="submit">Далее</button>
            <div id="CardRegError" class="card text-white bg-danger mt-2 anim-300 hidden">
                <div class="card-header">Внимание!</div>
                <div class="card-body">
                    <p class="card-text text-white">...</p>
                </div>
            </div>
        </form>
    </div>
</div>