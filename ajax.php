<?php
// Подключение набора функция по работе с базой данных
include_once 'db.php';



// Сессия
session_start();

if (isset($_GET['f'])) {
    if ($_GET['f'] == 'ajaxLogin') {
        ajaxLogin();
    }
    if ($_GET['f'] == 'ajaxReg') {
        ajaxReg();
    }
    if ($_GET['f'] == 'ajaxLogout') {
        ajaxLogout();
    }
    if ($_GET['f'] == 'ajaxUpdateUser') {
        ajaxUpdateUser();
    }

    /*
    if ($_GET['f'] == 'ajaxSetAnswer') {
        ajaxSetAnswer();
    }
    if ($_GET['f'] == 'ajaxRemoveTask') {
        ajaxRemoveTask();
    }
    */
}

/**
 * Авторизация пользователя в системе
 */
function ajaxLogin()
{
    $res = [];
    $res['status'] = 0;
    $res['data'] = [];
    $post = $_POST;
    if (!isset($post['email']) && !isset($post['password'])) {
        $res['status'] = 0;
        $res['data'] = "Не указан адрес электронной почты или пароль";
        print(json_encode($res));
        die();
    }

    $user = dbFindUser($post['email'], $post['password']);
    if ($user == false || count($user) == 0) {
        $res['status'] = 0;
        $res['data'] = "Пользователь не найден или введен не верный логин или пароль";
        print(json_encode($res));
        die();
    }

    $_SESSION['user'] =  $user;
    $res['status'] = 1;
    $res['data'] = $user;
    print(json_encode($res));
    die();
}




/**
 * Регистрация нового пользователя в системе
 */
function ajaxReg()
{
    $res = [];
    $res['status'] = 1;
    $res['data'] = [];
    $post = $_POST;
    $user = dbFindUser($post["email"]);
    if (is_array($user)) {
        $res['status'] = 0;
        $res['data'] = "Пользователь с таким почтовым адресом уже зарегистрирован";
    } else {
        if (dbCreateUser($post["email"], $post["password"], $post["name_first"], $post["name_last"], $post["role"], $post["phone"])) {
            $user = dbFindUser($post["email"], $post["password"]);
            $_SESSION['user'] =  $user;

            if ($user['role'] == 2) {
                if (isset($post["select_teachers"])) {
                    if (dbCreateGroup($post["select_teachers"], $user["id"])) {
                        $res['status'] = 1;
                        $res['data'] = $user;
                    } else {
                        $res['status'] = 0;
                        $res['data'] = "Не удалось прикрепить ученика к преподавателю";
                    }
                } else {
                    $res['status'] = 0;
                    $res['data'] = "Не указан преподаватель для студента.";
                }
            } else {
                $res['status'] = 1;
                $res['data'] = $user;
            }
        } else {
            $res['status'] = 0;
            $res['data'] = "Не удалось создать пользователя. Повторите попытку позже.";
        }
    }
    print(json_encode($res));
    die();
}

/**
 * AJAX. Выход пользователя из системы
 */
function ajaxLogout()
{

    $res = [];
    $res['status'] = 0;
    $res['data'] = [];
    session_start();
    $_SESSION = [];
    $res['status'] = 1;
    print(json_encode($res));
    die();
}

function ajaxUpdateUser()
{
    $res = [];
    $res['status'] = 0;
    $res['data'] = [];
    $post = $_POST;
    $id = $post['id'];
    $email = $post['email'];
    $password = $post['password'];
    $passwordRepeat = $post['password-repeat'];
    $nameFirst = $post['name_first'];
    $nameLast = $post['name_last'];
    $role = $post['role'];
    $phone = $post['phone'];

    if ($password != $passwordRepeat) {
        $res['status'] = 0;
        print(json_encode($res));
        die();
    }

    if (dbUpdateUser($id, $email, $password, $nameFirst, $nameLast, $role, $phone)) {
        $res['status'] = 1;
    }
    if (!dbRemoveGroup($id)) {
        $res['status'] = 0;
        $res['data'] = "Ошибка при попытки открепить ученика от учителя";
        print(json_encode($res));
        die();
    }
    if ($role == 2) {
        $teacherId = $post['select_teachers'];
        if (!dbCreateGroup($teacherId, $id)) {
            $res['status'] = 0;
            $res['data'] = "Ошибка при попытки прикрепить ученика к учителя";
            print(json_encode($res));
            die();
        }
    }

    $res['data'] = $post;
    print(json_encode($res));
    die();
}

/*
function ajaxSetAnswer()
{
    $h = $_GET['h'];
    $a = $_GET['a'];
    $v = $_GET['v'];
    $task = $_GET['task'];
    $res = [];
    $res['status'] = 0;
    $res['data'] = [];
    if (dbSetAnswer($task, $h, $a, $v)) {
        $res['status'] = 1;
    }
    echo json_encode($res);
    die();
}


function ajaxRemoveTask()
{
    session_start();
    $res = [];
    $res['status'] = 0;
    $res['data'] = [];

    $id = $_GET['id'];

    session_start();
    if ($_SESSION['user']['role'] != 1) {
        $res['status'] = 0;
        $res['data'] = ["Недостаточно прав на совершения данного действия"];
        echo json_encode($res);
        die();
    }
    if (dbRemoveTask($id) == false) {
        $res['status'] = 0;
        $res['data'] = ["Ошибка при выполнение запроса к базе данных"];
        echo json_encode($res);
        die();
    }
    $res['status'] = 1;
    $res['data'] = ["Задание удалено"];
    echo json_encode($res);
    die();
}
*/