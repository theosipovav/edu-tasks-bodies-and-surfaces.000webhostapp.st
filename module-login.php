<form id="FormSignIn" class="d-flex flex-column bg-white p-3" action="ajax.php?f=ajaxLogin" method="POST">
    <p class="h4 mb-4 text-dark">Авторизация</p>
    <input type="email" name="email" class="form-control mb-4" placeholder="Электронная почта" required>
    <input type="password" name="password" class="form-control mb-4" placeholder="Пароль" required>
    <div class="d-flex justify-content-around">
        <div style="display: none;">
            <a href="">Забыли пароль?</a>
        </div>
    </div>
    <button class="btn btn-primary" type="submit">Далее</button>
    <p class="text-dark text-center m-3">Нет аккаунта?
        <a href="?r=reg">Регистрация</a>
    </p>
    <div class="card card-error text-white bg-danger mt-3 mb-3 hidden">
        <div class="card-header">Внимание!</div>
        <div class="card-body">
            <h5 class="card-title">Не удалось выполнить авторизацию</h5>
            <p class="card-text text-white">...</p>
        </div>
    </div>
</form>