<?php
$users = dbGetUsers(2);
$tasks = dbGetTasks();
$answers = false;
if ($_SESSION['user']['role'] == 1) {
    $answers = getAnswers();
}
if ($_SESSION['user']['role'] == 2) {
    $answers = getAnswers($_SESSION['user']['id']);
}
?>
<div class="row bg-light">
    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <h1 class="h1">Результаты решения</h1>
            </div>
            <div class="col-6 d-flex justify-content-end mt-3 mb-3">
                <?php if (hasTeacher()) : ?>
                    <button type="submit" name="submit" form="FromAnswerFilter" class="btn btn-primary m-1">Фильтр</button>
                    <a href="<?= $_SERVER['REQUEST_URI'] ?>" class="btn btn-outline-primary m-1">Сбросить фильтр</a>
                <?php endif ?>
            </div>
        </div>
        <div class=" heavy-rain-gradient z-depth-3">
            <div class="row p-3">
                <div class="col-12 col-md-1 text-center">
                    <b>№</b>
                </div>
                <div class="col-12 col-md-4 text-center">
                    <b>Ученик</b>
                </div>
                <div class="col-12 col-md-4 text-center">
                    <b>Задание</b>
                </div>
                <div class="col-12 col-md-3 text-center">
                    <b>Результат</b>
                </div>
            </div>
            <?php if ($_SESSION['user']['role'] == 1) : ?>
                <form id="FromAnswerFilter" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post" class="mb-3">
                    <div class="row">
                        <div class="col-12 col-md-1 text-center"></div>
                        <div class="col-12 col-md-4 text-center">
                            <select name="user-id" class="form-control">
                                <option value="-1">Все студенты</option>
                                <?php
                                foreach ($users as $key => $user) {
                                    if (isset($_POST['submit'])) {
                                        if ($user['id'] == $_POST['user-id']) {
                                            echo '<option value="' . $user['id'] . '" selected>' . $user['username'] . '</option>';
                                        } else {
                                            echo '<option value="' . $user['id'] . '">' . $user['username'] . '</option>';
                                        }
                                    } else {
                                        echo '<option value="' . $user['id'] . '">' . $user['username'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-12 col-md-4 text-center">
                            <select name="task-id" class="form-control">
                                <option value="-1">Все задания</option>

                                <?php


                                foreach ($tasks as $key => $task) {
                                    if (isset($_POST['submit'])) {
                                        if ($task->id == $_POST['task-id']) {
                                            echo '<option value="' . $task->id  . '" selected>' . $task->id  . '</option>';
                                        } else {
                                            echo '<option value="' . $task->id  . '">' . $task->id  . '</option>';
                                        }
                                    } else {
                                        echo '<option value="' . $task->id  . '">' . $task->id  . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-12 col-md-3 text-center">
                            <select name="answer-result" class="form-control">
                                <option value="-1">Все результаты</option>
                                <?PHP
                                if (isset($_POST['submit'])) {
                                    if ($_POST['answer-result'] == -1) {
                                        echo '<option value="0" >Ошибка</option>';
                                        echo '<option value="1">Верно</option>';
                                    }
                                    if ($_POST['answer-result'] == 0) {
                                        echo '<option value="0" selected>Ошибка</option>';
                                        echo '<option value="1">Верно</option>';
                                    }
                                    if ($_POST['answer-result'] == 1) {
                                        echo '<option value="0">Ошибка</option>';
                                        echo '<option value="1" selected>Верно</option>';
                                    }
                                } else {
                                    echo '<option value="1">Верно</option>';
                                    echo '<option value="0">Ошибка</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </form>
            <?php endif ?>
        </div>
        <div class="">
            <?php foreach ($answers as $key => $answer) : ?>
                <?php
                if (isset($_POST['submit'])) {
                    $post = $_POST;
                    if (isset($post['user-id'])) {
                        if ($post['user-id'] != '-1' && $answer->user_id != $post['user-id']) {
                            continue;
                        }
                    }
                    if (isset($post['task-id'])) {
                        if ($post['task-id'] != '-1' && $answer->task_id != $post['task-id']) {
                            continue;
                        }
                    }
                    if (isset($post['answer-result'])) {
                        if ($post['answer-result'] != '-1') {
                            if ($post['answer-result'] != '0' && !checkResultTask($answer->id)) {
                                continue;
                            }
                            if ($post['answer-result'] != '1' && checkResultTask($answer->id)) {
                                continue;
                            }
                        }
                    }
                }
                if (checkResultTask($answer->id)) {
                    $css = "bg-success";
                } else {
                    $css =  "bg-danger";
                }
                ?>


                <div class="row <?= $css ?> text-light pt-2 pb-2">
                    <div class="col-1 text-center"><?= $key ?></div>
                    <div class="col-4 text-center"><?= dbGetNameUser($answer->user_id) ?></div>
                    <div class="col-4 text-center"><?= $answer->task_id ?></div>
                    <div class="col-3 text-center"></div>
                </div>
            <?php endforeach ?>
        </div>
    </div>

</div>